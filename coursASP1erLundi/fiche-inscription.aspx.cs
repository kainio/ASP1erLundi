﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace coursASP1erLundi
{
    public partial class fiche_inscription : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ClientScript.RegisterOnSubmitStatement(this.GetType(), "script", "confirm(SubmitMessage())");

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Label6.Text = "Les informations sont Correctes";
                string resultat = String.Format("confirm(\"Nom complet: {0} {1}\\nAge: {2}\\nAdresse email: {3}\\nTel: {4}\");", TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text);
                ClientScript.RegisterStartupScript(this.GetType(), "script", resultat, true);
            }
        }
    }
}