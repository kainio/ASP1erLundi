﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="coursASP1erLundi._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Nom"></asp:Label><br />
            <asp:TextBox ID="txtnom" runat="server"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtnom" ErrorMessage="admin n'est pas un nom valide" InitialValue="admin"></asp:RequiredFieldValidator><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"  ControlToValidate="txtnom" ErrorMessage="champ est obligatoire" ></asp:RequiredFieldValidator><br />
            
            <asp:TextBox ID="txt1" runat="server"></asp:TextBox><br />
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txt1" Type="Integer" Operator="DataTypeCheck" ErrorMessage="CompareValidator"></asp:CompareValidator><br />
            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txt1" Type="Integer" Operator="GreaterThan" ValueToCompare="0" ErrorMessage="CompareValidator"></asp:CompareValidator><br />
            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txt1" Type="Integer" Operator="Equal" ControlToCompare="txtnom" ErrorMessage="c'est pas le meme"></asp:CompareValidator><br />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txt1" ValidationExpression="^\d{3}\d{7}\d{2}" runat="server" ErrorMessage="RegularExpressionValidator"></asp:RegularExpressionValidator><br />
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txt1" Type="integer" MinimumValue="0" MaximumValue="10"  ErrorMessage="La valeur doit être comprise entre 0 et 10"></asp:RangeValidator><br />
            
            <asp:TextBox ID="num1" runat="server"></asp:TextBox><br />
            <asp:RegulareExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="num1" ValidationExpression=""></asp:RegulareExpressionValidator><br />
            <asp:Button ID="Button1" style="margin-left:10px" runat="server" Text="ok" />

         
        </div>
        
    </form>
   
</body>
</html>
