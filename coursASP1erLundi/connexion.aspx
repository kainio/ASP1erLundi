﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="connexion.aspx.cs" Inherits="coursASP1erLundi.connexion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="label1" runat="server" Text="Mot de passe"></asp:Label><br />
            <asp:TextBox ID="txt1" runat="server" TextMode="Password" ></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Text="*" runat="server" ControlToValidate="txt1" ErrorMessage="Champ obligatoire"></asp:RequiredFieldValidator>
            <%--<asp:RegularExpressionValidator runat="server" Text="*" ForeColor="Red" ValidationExpression=".*\d.*" ControlToValidate="txt1" ErrorMessage="Mot de passe doit contenir au moins un chiffre"></asp:RegularExpressionValidator><br />
            <asp:RegularExpressionValidator runat="server" Text="*" ForeColor="Red" ValidationExpression=".{5,10}" ControlToValidate="txt1" ErrorMessage="Mot de passe doit contenir entre 5 et 10 caractères"></asp:RegularExpressionValidator><br />--%>
            <asp:RegularExpressionValidator runat="server" Text="*" ForeColor="Red" ValidationExpression="(?=.*[\d]).{5,10}" ControlToValidate="txt1" ErrorMessage="Le mot de passe n'est pas valide"></asp:RegularExpressionValidator><br />
            <asp:Label ID="label2" runat="server" Text="Confirmation du mot de passe"></asp:Label><br />
            <asp:TextBox ID="txt2" runat="server" TextMode="Password" ></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Text="*" runat="server" ControlToValidate="txt2" ErrorMessage="Champ obligatoire"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="CompareValidator1" Text="*" ForeColor="Red" ControlToValidate="txt2" ControlToCompare="txt1" ErrorMessage="Le mot de passe saisie ne corresponds pas"></asp:CompareValidator><br />
            <asp:Button ID="button1" Text="Valider" OnClick="btn_validation" runat="server" /><br />
            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        </div>
    </form>
</body>
</html>
