﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="fiche-inscription.aspx.cs" Inherits="coursASP1erLundi.fiche_inscription" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Inscription</title>
</head>
<body>
    <h1>Fiche D'inscription</h1>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Nom"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" Text="*" ErrorMessage="Champ Obligatoire"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressonValidator1" runat="server" ControlToValidate="TextBox1"  ValidationExpression="[a-zA-Z\s]+" Text="*" ErrorMessage="Vous devez saisir une chaine de caractères seulement"></asp:RegularExpressionValidator><br />
            <%--<asp:CompareValidator ID="CompareValidator1" runat="server" Type="String" ControlToValidate="TextBox1" Operator="DataTypeCheck" Text="*" ErrorMessage="Vous devez saisir une chaine de caractères seulement"></asp:CompareValidator><br />--%>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br />


            <asp:Label ID="Label2" runat="server" Text="Prenom"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox2" Text="*" ErrorMessage="Champ Obligatoire"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox2"  ValidationExpression="[a-zA-Z\s]+" Text="*" ErrorMessage="Vous devez saisir une chaine de caractères seulement"></asp:RegularExpressionValidator><br />
            <%--<asp:CompareValidator ID="CompareValidator2" runat="server" Type="String" ControlToValidate="TextBox2" Operator="DataTypeCheck" Text="*" ErrorMessage="Vous devez saisir une chaine de caractères seulement"></asp:CompareValidator><br />--%>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><br />

            <asp:Label ID="Label3" runat="server" Text="Age"></asp:Label>
            <asp:RangeValidator ID="RangeValidator1" runat="server" Type="Integer" ControlToValidate="TexTBox3" MinimumValue="18" MaximumValue="100" Text="*" ErrorMessage="L'age doit être supérieur à 18ans"></asp:RangeValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox3" Text="*" ErrorMessage="Champ Obligatoire"></asp:RequiredFieldValidator><br />
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox><br />
            
            <asp:Label ID="Label4" runat="server" Text="Adresse email"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox4" Text="*" ErrorMessage="Champ Obligatoire"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegulareExpressionValidator3" runat="server" ControlToValidate="TextBox4" ValidationExpression="^.+@[a-zA-Z]+\.[a-zA-Z]{3}$" Text="*" ErrorMessage="Adresse email non valide"></asp:RegularExpressionValidator><br />
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><br />

            <asp:Label ID="Label5" runat="server" Text="Tel"></asp:Label>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBox5" ValidationExpression="^\+\d{3}(\.(\d){2}){5}$" Text="*" ErrorMessage="Numéro de téléphone non valide"></asp:RegularExpressionValidator><br />
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><br />

            <asp:Button ID="Button1" runat="server" Text="Envoyer" OnClick="Button1_Click" /><br />
            <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server"/>
            <script type="text/javascript">
                function SubmitMessage() {
                    var nom = document.getElementById("TextBox1").value;
                    var prenom = document.getElementById("TextBox2").value;
                    var age = document.getElementById("TextBox3").value;
                    var email = document.getElementById("TextBox4").value;
                    var tel = document.getElementById("TextBox5").value;

                    return "Nom complet: " + nom + " " + prenom + "\nAge: " + age + "\nAdresse email: " + email + "\nTel: " + tel; 

                }
            </script>
        </div>
    </form>
</body>
</html>
